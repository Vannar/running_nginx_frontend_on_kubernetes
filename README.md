# Deploying a Frontend Container on Kubernetes

This guide will walk you through the steps to deploy a frontend container on Kubernetes. For demo purposes, we will be using an Azure Kubernetes Cluster and a publicly available Docker image from Docker Hub. You can substitute these with your own Kubernetes cluster and Docker image.

## Prerequisites

Before we begin, you will need the following:

A Kubernetes cluster
The kubectl command-line tool installed and configured to connect to your Kubernetes cluster

# Steps

1.) Clone the repository and navigate to the project directory:
    
    git clone <repository-url>
    cd <project-directory>

2.) Apply the Kubernetes manifest file to create the pod, service, and deployment for the frontend container:
 
    kubectl apply -f manifest.yaml

3.) Wait for a few minutes for the resources to be created. You can check the status of the resources using the following commands:

    kubectl get pods
    kubectl get svc
    kubectl get deployments

These commands will show you the status of the pods, services, and deployments created by the manifest file.

4.) Once the resources are created, you can access the frontend container using the IP address of the service. You can find the IP address using the following command:
 
    kubectl get svc
This command will show you the IP address of the service created by the manifest file. You can use this IP address to access the frontend container.

# Conclusion

In this guide, we learned how to deploy a frontend container on Kubernetes. We used an Azure Kubernetes Cluster and a publicly available Docker image from Docker Hub for demo purposes, but you can substitute these with your own Kubernetes cluster and Docker image.
